<?php

require_once 'Util.php';
class CoreAPI
{
    private $PDO;
    private $util;

    public function __construct($host = 'localhost', $db = 'quickvote', $user = 'api', $password = 'bananaIsTheWord28')
    {
        $this->util = new Util();

        $dsn = 'mysql:host=' . $host . ';dbname=' . $db . ';charset=utf8';

        try {
            $this->PDO = new PDO($dsn, $user, $password);
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }


    }


    //  User related functions

    public function addUser($rawUsername, $rawPass, $rawEmail)
    {

        $salt = $this->util->genRandomString(64);


        $user = $this->util->filter($rawUsername);
        $pass = hash('sha256', $this->util->filter($rawPass) . $salt);
        $email = $this->util->filter($rawEmail);

        $query = $this->PDO->prepare('INSERT INTO User (username, password, email, salt) VALUES (:usr, :pass, :mail, :salt)');
        $query->execute(array(
            ':usr' => $user,
            ':pass' => $pass,
            ':mail' => $email,
            ':salt' => $salt
        ));

        return true;
    }

    public function delUser($rawUsername, $userToken)
    {
        if (!$this->validateToken($userToken)) {
            return false;
        } else {
            $user = $this->util->filter($rawUsername);

            $query = $this->PDO->prepare('DELETE FROM User WHERE username=:usr');
            $query->execute(array(
                ':usr' => $user
            ));

            return true;
        }

    }

    private function validateToken($token)
    {
        $statement = $this->PDO->prepare("SELECT EXISTS (SELECT * FROM Token WHERE `token_str`=:tkn)");

        $statement->execute(array(
            ':tkn' => $token
        ));

        $result = $statement->fetch(PDO::FETCH_BOTH);

        if ($result[0] = 1) {
            return true;
        } else {
            return false;
        }



    }

    public function addGuestUser()
    {
        try {
            $username = "Guest_" . time() * 1000;


            $token = $this->util->genRandomString(64);

            $query = $this->PDO->prepare("INSERT INTO Token(token_str) VALUES (:token)");

            $query->execute(array(
                ':token' => $token
            ));


            $query = $this->PDO->prepare('INSERT INTO User (username, isGuest) VALUES (:usr, 1)');
            $query->execute(array(
                ':usr' => $username
            ));


            $info = array(
                'username' => $username,
                'token' => $token
            );

            return $info;  //TODO add proper return of token

        } catch (Exception $e) {
            print $e->getMessage();
        }


    }


    // Questions related functions

    public function addQuestion($questionStr, $commentStr, $isHidden, $acceptsMany, $isClosed, $questionType, $isPrivate, $password = null, $ownerID, $tags, $answers, $userToken)
    {
        if ($this->validateToken($userToken) == true) {

            $statement = $this->PDO->prepare("INSERT INTO question(question, commentaire, hidden, many, closed, type, private, pass, owner_id, salt) VALUES (:quest, :cmnt, :hid, :man, :clo, :type, :prv, :pss, :own, :slt)");

            if ($password == null) {
                $salt = null;
                $passHash = null;
            } else {
                $salt = openssl_random_pseudo_bytes(32, $secure);
                $passHash = hash('sha256', $this->util->filter($password) . $salt);
            }

            $statement->execute(array(
                ':quest' => $this->util->filter($questionStr),
                ':cmnt' => $this->util->filter($commentStr),
                ':hid' => $this->util->filter($isHidden),
                ':man' => $this->util->filter($acceptsMany),
                ':clo' => $this->util->filter($isClosed),
                ':type' => $this->util->filter($questionType),
                ':prv' => $this->util->filter($isPrivate),
                ':pss' => $passHash,
                ':own' => $ownerID,
                ':slt' => $salt
            ));

            $questionID = $this->PDO->lastInsertId();
            $this->addTags($questionID, $tags);

            $this->addQuestionAnswer($questionID, $answers);

            return true;

        } else {
            return false;
        }
    }

    private function addTags($questionId, $tags)
    {

        foreach ($tags as $tag) {
            $statement = $this->PDO->prepare("INSERT INTO Tag(name) VALUES (:tag)");

            $statement->execute(array(
                ':tag' => $tag
            ));
        }

        foreach ($tags as $tag) {
            $statement = $this->PDO->prepare("INSERT INTO QuestionTag(quest_id, tag) VALUES (:quest, :tag)");

            $statement->execute(array(
                ':quest' => $questionId,
                ':tag' => $tag
            ));
        }

    }

    private function addQuestionAnswer($questID, $answers)
    {

        foreach ($answers as $answer) {
            $statement = $this->PDO->prepare("INSERT INTO answer(quest_id, text) VALUES (:id, :txt)");

            $statement->execute(array(
                ':id' => $questID,
                ':txt' => $answer
            ));
        }

    }

    public function delQuestion($questID, $userID, $userToken)
    {
        if ($this->validateToken($userToken) == true) {

            $statement = $this->PDO->prepare("SELECT owner_id FROM Question WHERE id=:id");
            $statement->execute(array(
                ':id' => $questID
            ));

            $result = $statement->fetch(PDO::FETCH_ASSOC);

            if ($result['owner_id'] == $userID) {

                // `hidden` = '1' `closed` = '1' WHERE `Question`.`id` =1;

                $statement = $this->PDO->prepare("DELETE FROM Question WHERE id=:id");
                $statement->execute(array(
                    ':id' => $questID
                ));

                return true;


            } else {
                return false;
            }

        } else {
            return false;
        }

    }

    public function getQuestionAnswers($questionID, $userToken)
    {

        if ($this->validateToken($userToken) == true) {
            $statement = $this->PDO->prepare("SELECT text FROM Answer WHERE quest_id=:questID");

            $result = $statement->execute(array(
                ':questID' => $questionID
            ));

            return $result;
        } else {
            return false;
        }


    }

    public function answerQuestion($questionID, $rawAnswerText, $userID, $userToken)
    {
        if ($this->validateToken($userToken)) {

            $statement = $this->PDO->prepare("INSERT INTO UserAnswer(usr_id, quest_id, answer_text) VALUES (:uid, :qid, :ans)");

            $statement->execute(array(
                ':uid' => $this->util->filter($userID),
                ':qid' => $questionID,
                ':ans' => $this->util->filter($rawAnswerText)
            ));

            return true;

        } else {
            return false;
        }

    }

    public function modParamQuestion($questID, $userID, $params, $userToken)
    {
        if ($this->validateToken($userToken)) {

            $statement = $this->PDO->prepare("SELECT owner_id FROM Question WHERE id=:id");
            $statement->execute(array(
                ':id' => $questID
            ));

            $result = $statement->fetch(PDO::FETCH_ASSOC);

            if ($result['owner_id'] == $userID) {

                // `hidden` = '1' `closed` = '1' WHERE `Question`.`id` =1;

                $statement = $this->PDO->prepare("UPDATE Question SET hidden=:hdn, closed=:clo");
                $statement->execute(array(
                    ':hdn' => $params['hidden'],
                    ':clo' => $params['closed']
                ));

                return true;


            } else {
                return false;
            }


        } else {
            return false;
        }


    }

    public function getQuestionStats($questID, $questPass = null, $userToken)
    {

        if ($this->validateToken($userToken)) {
            if ($this->checkQuestionAccess($questID, $questPass)) {

                $statement = $this->PDO->prepare("SELECT answer_text, COUNT(answer_text) AS nbr_answer FROM UserAnswer WHERE quest_id=:id GROUP BY answer_text");
                $statement->execute(array(
                    ':id' => $questID
                ));

                $tab = array();

                while ($donnee = $statement->fetch()) {
                    $tab[$donnee['answer_text']] = $donnee['nbr_answer'];
                }

                return $tab;    //TODO parse $tab in JSON

            } else {
                return false;
            }
        }else{
            return false;
        }


    }

    private function checkQuestionAccess($questID, $pass = null)
    {

        $statement = $this->PDO->prepare('SELECT * FROM Question WHERE id=:id');

        $statement->execute(array(
            ':id' => $questID
        ));


        $data = $statement->fetch();

        if (!$data) {
            return false;
        } else {
            $passHash = $data['pass'];
            $salt = $data['salt'];
            $private = $data['private'];
        }

        if (!$private) {
            if ($pass == null) {
                return true;
            } else {
                return false;
            }
        }

        $hash = hash('sha256', $pass . $salt);

        if (strcmp($hash, $passHash) == 0) {
            return true;
        } else {
            return false;
        }


    }

    public function getQuestion($questID)
    {

        $statement = $this->PDO->prepare("SELECT * FROM Question WHERE quest_id=:id");

        $statement->execute(array(
            ':id'   =>  $questID
        ));

        $questionRow = $statement->fetch(PDO::FETCH_ASSOC);

        return $questionRow;


    }


    //Token related methods

    public function modPassQuestion($questID, $userID, $pass, $token)
    {
        if ($this->validateToken($token) == true) {
            if ($pass == "") {
                $statement = $this->PDO->prepare("SELECT owner_id FROM Question WHERE id=:id");
                $statement->execute(array(
                    ':id' => $questID
                ));

                $result = $statement->fetch(PDO::FETCH_ASSOC);

                if ($result['owner_id'] == $userID) {

                    $statement = $this->PDO->prepare("UPDATE Question SET pass=null, salt=null, private=0 WHERE id=:id");
                    $statement->execute(array(
                        'id' => $questID
                    ));

                    return true;


                } else {
                    return false;
                }
            } else {
                $salt = openssl_random_pseudo_bytes(32, $secure);
                $passHash = hash('sha256', $this->util->filter($pass) . $salt);

                $statement = $this->PDO->prepare("SELECT owner_id FROM Question WHERE id=:id");
                $statement->execute(array(
                    ':id' => $questID
                ));

                $result = $statement->fetch(PDO::FETCH_ASSOC);

                if ($result['owner_id'] == $userID) {

                    $statement = $this->PDO->prepare("UPDATE Question SET pass=:pass, salt=:salt, private=1 WHERE id=:id");
                    $statement->execute(array(
                        ':pass' => $passHash,
                        ':salt' => $salt,
                        'id' => $questID
                    ));

                    return true;


                } else {
                    return false;
                }
            }
        } else {
            return false;
        }


    }

    public function login($username, $password)
    {
        if ($this->checkUser($username, $password)) {
            $token = $this->util->genRandomString(64);

            $query = $this->PDO->prepare("INSERT INTO Token(token_str) VALUES (:token)");

            $query->execute(array(
                ':token' => $token
            ));

            $info = array(
                ':username' => $username,
                ':token' => $token
            );

            return $info;

        } else {
            return false;
        }
    }


    //Tag related methods

    private function checkUser($rawUser, $rawPass)
    {
        $user = $this->util->filter($rawUser);
        $password = $this->util->filter($rawPass);

        $statement = $this->PDO->prepare('SELECT password, salt FROM User WHERE username=:usr');

        $statement->execute(array(
            ':usr' => $user
        ));


        $data = $statement->fetch();


        if (!$data) {
            return false;
        } else {
            $passHash = $data[0];
            $salt = $data[1];
        }

        $hash = hash('sha256', $password . $salt);


        if (strcmp($hash, $passHash) == 0) {
            return true;
        } else {
            return false;
        }


    }


    //Search related functions

    public function searchByTag($searchString, $tags, $params, $sorting)
    {

        $tagQuery = "SELECT t1.* FROM (SELECT * FROM Question) AS t1 JOIN (SELECT * FROM QuestionTag) AS t2 ON t1.id = t2.quest_id WHERE";

        for ($i = 0; $i < sizeof($tags); $i++) {
            $tagQuery = $tagQuery . " tag='" . $tags[$i] . "'";
            if ($i != sizeof($tags) - 1) {
                $tagQuery = $tagQuery . " OR";
            }

        }

        if ($params == null) {
            $searchConditionsString = "";
        } else {
            $trueParams = array();
            $searchConditionsString = "AND ";


            $i = 0;
            foreach ($params as $paramName => $paramValue) {
                if ($params[$paramName] == 1) {
                    $trueParams[$i] = $paramName;
                }
                $i++;
            }

            for ($i = 0; $i < sizeof($trueParams); $i++) {
                $searchConditionsString .= $trueParams[$i] . "=1 ";
                if ($i != sizeof($trueParams) - 1) {
                    $searchConditionsString .= "AND ";
                }
            }
        }

        switch ($sorting) {
            case "ABC" :
                $statement = $this->PDO->prepare("SELECT * FROM (" . $tagQuery . ") AS t1 WHERE (question LIKE :srcstr) " . $searchConditionsString . " ORDER BY question");
                break;
            case "ZYX" :
                $statement = $this->PDO->prepare("SELECT * FROM (" . $tagQuery . ") AS t1 WHERE (question LIKE :srcstr) " . $searchConditionsString . " ORDER BY question DESC");
                break;
            case "New" :
                $statement = $this->PDO->prepare("SELECT * FROM (" . $tagQuery . ") AS t1 WHERE (question LIKE :srcstr) " . $searchConditionsString . " ORDER BY creation_date");
                break;
            case "Old" :
                $statement = $this->PDO->prepare("SELECT * FROM (" . $tagQuery . ") AS t1 WHERE (question LIKE :srcstr) " . $searchConditionsString . " ORDER BY creation_date DESC");
                break;
            case "Pop" :
                // SELECT t1.*, COUNT(*) AS nbr_answer FROM (SELECT * FROM Question) AS t1 INNER JOIN (SELECT usr_id, quest_id, answer_text FROM UserAnswer) as t2 ON t1.id = t2.quest_id GROUP BY id
                //Please Jesus make this query work !!!
                $statement = $this->PDO->prepare("SELECT `id`, `question`, `commentaire`, `hidden`, `many`, `closed`, `type`, `private`, `pass`, `owner_id`, `salt`, `creation_date` FROM 
                                                  (SELECT t1.*, COUNT(*) AS nbr_answer FROM (SELECT * FROM (" . $tagQuery . ") AS t3) AS t1 INNER JOIN (SELECT usr_id, quest_id, answer_text FROM UserAnswer) as t2 
                                                    ON t1.id = t2.quest_id GROUP BY id ORDER BY nbr_answer DESC) t WHERE (question LIKE :srcstr) " . $searchConditionsString);
                break;
            default :
                $statement = $this->PDO->prepare("SELECT * FROM (" . $tagQuery . ") AS t1 WHERE (question LIKE :srcstr) " . $searchConditionsString . " ORDER BY question");
                break;
        }


        $answer = $statement->execute(array(
            ':srcstr' => "%" . $this->util->filter($searchString) . "%"
        ));

        $answer = array();

        $i = 0;
        while (($row = $statement->fetch(PDO::FETCH_ASSOC)) != null) {
            $answer[$i] = $row;
            $i++;
        }

        echo json_encode($answer);

        return $answer;



    }

    public function searchQuestionByString($searchString, $params, $sorting)
    {
        if ($params == null) {
            $searchConditionsString = "";
        } else {
            $trueParams = array();
            $searchConditionsString = "AND ";


            $i = 0;
            foreach ($params as $paramName => $paramValue) {
                if ($params[$paramName] == 1) {
                    $trueParams[$i] = $paramName;
                }
                $i++;
            }

            for ($i = 0; $i < sizeof($trueParams); $i++) {
                $searchConditionsString .= $trueParams[$i] . "=1 ";
                if ($i != sizeof($trueParams) - 1) {
                    $searchConditionsString .= "AND ";
                }
            }
        }

        switch ($sorting) {
            case "ABC" :
                $statement = $this->PDO->prepare("SELECT * FROM Question WHERE (question LIKE :srcstr) " . $searchConditionsString . " ORDER BY question");
                break;
            case "ZYX" :
                $statement = $this->PDO->prepare("SELECT * FROM Question WHERE (question LIKE :srcstr) " . $searchConditionsString . " ORDER BY question DESC");
                break;
            case "New" :
                $statement = $this->PDO->prepare("SELECT * FROM Question WHERE (question LIKE :srcstr) " . $searchConditionsString . " ORDER BY creation_date DESC");
                break;
            case "Old" :
                $statement = $this->PDO->prepare("SELECT * FROM Question WHERE (question LIKE :srcstr) " . $searchConditionsString . " ORDER BY creation_date");
                break;
            case "Pop" :
                // SELECT t1.*, COUNT(*) AS nbr_answer FROM (SELECT * FROM Question) AS t1 INNER JOIN (SELECT usr_id, quest_id, answer_text FROM UserAnswer) as t2 ON t1.id = t2.quest_id GROUP BY id
                //Please Jesus make this query work !!!
                $statement = $this->PDO->prepare("SELECT `id`, `question`, `commentaire`, `hidden`, `many`, `closed`, `type`, `private`, `pass`, `owner_id`, `salt`, `creation_date` FROM 
                                                  (SELECT t1.*, COUNT(*) AS nbr_answer FROM (SELECT * FROM Question) AS t1 INNER JOIN (SELECT usr_id, quest_id, answer_text FROM UserAnswer) as t2 
                                                    ON t1.id = t2.quest_id GROUP BY id ORDER BY nbr_answer DESC) t WHERE (question LIKE :srcstr) " . $searchConditionsString);
                break;
            default :
                $statement = $this->PDO->prepare("SELECT * FROM Question WHERE (question LIKE :srcstr) " . $searchConditionsString);
                break;
        }


        $statement->execute(array(
            ':srcstr' => "%" . $this->util->filter($searchString) . "%"
        ));

        $answer = array();

        $i = 0;
        while (($row = $statement->fetch(PDO::FETCH_ASSOC)) != null) {
            $answer[$i] = $row;
            $i++;
        }

        echo json_encode($answer);
        
        return $answer;

    }

}



