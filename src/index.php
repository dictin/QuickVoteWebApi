<?php

/**
 * Created by PhpStorm.
 * User: dictin
 * Date: 17/03/16
 * Time: 9:47 PM
 */
require_once 'CoreAPI.php';

header('Content-type: application/json');

define("ACCESS_KEY", "zeg0d58lNZ0xOIQg1wuwtRwiWUOh3oaAbBGHWDitJuJALl4OKibYYeo74bieDdEF");

class Frontend
{

    private $API;

    public function __construct()
    {

        $this->API = new CoreAPI();
    }

    public function checkForValidParams()
    {
        $action = $_GET['action'];

        switch ($action) {
            case "addUser":
                if ((isset($_GET['username'])) && (isset($_GET['password'])) && (isset($_GET['email']))) {
                    $answer = $this->API->addUser($_GET['username'], $_GET['password'], $_GET['email']);
                    echo json_encode($answer);
                } else {
                    echo json_encode("Missing parameter for 'addUser' action");
                }
                break;
            case "login":
                if ((isset($_GET['username'])) && (isset($_GET['password']))) {
                    $answer = $this->API->login($_GET['username'], $_GET['password']);
                    echo json_encode($answer);
                } else {
                    echo json_encode("Missing parameter for 'login' action");
                }
                break;
            case "delUser":
                if ((isset($_GET['username'])) && (isset($_GET['token']))) {
                    $answer = $this->API->delUser($_GET['username'], $_GET['token']);
                    echo json_encode($answer);
                } else {
                    echo json_encode("Missing parameter for 'delUser' action");
                }
                break;
            case "addGuestUser":
                $answer = $this->API->addGuestUser();
                echo json_encode($answer);
                break;
            case "addQuestion":
                if ((isset($_GET['username'])) && (isset($_GET['token'])) && (isset($_GET['args']))) {
                    $json_args = json_decode($_GET['args']);
                    $answer = $this->API->addQuestion($json_args->questionStr, $json_args->comment, $json_args->isHidden, $json_args->acceptsMany, $json_args->isClosed, $json_args->questionType, $json_args->isPrivate, $json_args->password, $_GET['username'], $json_args->tags, $_GET['token']);
                    echo json_encode($answer);
                } else {
                    echo json_encode("Missing parameter for 'addQuestion' action");
                }
                break;
            case "delQuestion":
                if ((isset($_GET['username'])) && (isset($_GET['token'])) && (isset($_GET['questID']))) {
                    $answer = $this->API->delQuestion($_GET['questID'], $_GET['username'], $_GET['token']);
                    echo json_encode($answer);
                } else {
                    echo json_encode("Missing parameter for 'delQuestion' action");
                }
                break;
            case "modPassQuestion":
                if ((isset($_GET['username'])) && (isset($_GET['token'])) && (isset($_GET['questID'])) && (isset($_GET['password']))) {
                    $answer = $this->API->modPassQuestion($_GET['questID'], $_GET['username'], $_GET['password'], $_GET['token']);
                    echo json_encode($answer);
                } else {
                    echo json_encode("Missing parameter for 'modPassQuestion' action");
                }
                break;
            case "getQuestionAnswers":
                if ((isset($_GET['token'])) && (isset($_GET['questID']))) {
                    $answer = $this->API->getQuestionAnswers($_GET['questID'], $_GET['token']);
                    echo json_encode($answer);
                } else {
                    echo json_encode("Missing parameter for 'getQuestionAnswers' action");
                }
                break;
            case "answerQuestion":
                if ((isset($_GET['username'])) && (isset($_GET['token'])) && (isset($_GET['questID'])) && (isset($_GET['text']))) {
                    $answer = $this->API->answerQuestion($_GET['questID'], $_GET['text'], $_GET['username'], $_GET['token']);
                    echo json_encode($answer);
                } else {
                    echo json_encode("Missing parameter for 'answerQuestion' action");
                }
                break;
            case "modParamQuestion":
                if ((isset($_GET['username'])) && (isset($_GET['token'])) && (isset($_GET['questID'])) && (isset($_GET['params']))) {
                    $params = json_decode($_GET['params'])->param;
                    $answer = $this->API->modParamQuestion($_GET['username'], $_GET['username'], $params, $_GET['token']);
                    echo json_encode($answer);
                } else {
                    echo json_encode("Missing parameter for 'modParamQuestion' action");
                }
                break;
            case "getQuestionStats":
                if ((isset($_GET['token'])) && (isset($_GET['questID'])) && (isset($_GET['password']))) {
                    $answer = $this->API->getQuestionStats($_GET['questID'], $_GET['password'], $_GET['token']);
                    echo json_encode($answer);
                } else {
                    echo json_encode("Missing parameter for 'getQuestionStats' action");
                }
                break;
            case "getQuestion":
                if (isset($_GET['questID'])) {
                    $answer = $this->API->getQuestion($_GET['questID']);
                    echo json_encode($answer);
                } else {
                    echo json_encode("Missing parameter for 'getQuestion' action");
                }
                break;
            case "search":
                if ((isset($_GET['string']))) {
                    if ((isset($_GET['params'])) && (isset($_GET['sorting']))) {
                        $params_obj = json_encode($_GET['params']);
                        $answer = $this->API->searchQuestionByString($_GET['string'], $params_obj->param, $_GET['sorting']);
                        echo json_encode($answer);
                    } elseif ((isset($_GET['params'])) && (!isset($_GET['sorting']))) {
                        $params_obj = json_encode($_GET['params']);
                        $answer = $this->API->searchQuestionByString($_GET['string'], $params_obj->param, "");
                        echo json_encode($answer);
                    } elseif ((!isset($_GET['params'])) && (isset($_GET['sorting']))) {
                        $answer = $this->API->searchQuestionByString($_GET['string'], null, $_GET['sorting']);
                        echo json_encode($answer);
                    } else {
                        $answer = $this->API->searchQuestionByString($_GET['string'], null, "");
                        echo json_encode($answer);
                    }
                } else {
                    echo json_encode("Missing parameter for 'search' action");
                }
                break;
            case "searchByTag":
                if ((isset($_GET['string'])) && (isset($_GET['tags']))) {
                    if ((isset($_GET['param'])) && (isset($_GET['sorting']))) {
                        $params_obj = json_encode($_GET['params']);
                        $tags_obj = json_encode($_GET['tags']);
                        $answer = $this->API->searchByTag($_GET['string'], $tags_obj->tags, $params_obj->param, $_GET['sorting']);
                        echo json_encode($answer);
                    } elseif ((isset($_GET['param'])) && (!isset($_GET['sorting']))) {
                        $params_obj = json_encode($_GET['params']);
                        $tags_obj = json_encode($_GET['tags']);
                        $answer = $this->API->searchByTag($_GET['string'], $tags_obj->tags, $params_obj->param, "");
                        echo json_encode($answer);
                    } elseif ((!isset($_GET['param'])) && (isset($_GET['sorting']))) {
                        $tags_obj = json_encode($_GET['tags']);
                        $answer = $this->API->searchByTag($_GET['string'], $tags_obj->tags, null, $_GET['sorting']);
                        echo json_encode($answer);
                    } else {
                        $tags_obj = json_encode($_GET['tags']);
                        $answer = $this->API->searchByTag($_GET['string'], $tags_obj->tags, null, "");
                        echo json_encode($answer);
                    }
                } else {
                    echo json_encode("Missing parameter for 'searchByTag' action");
                }
                break;
            default:
                $ret = "Unknown action '" . $action . "'";
                echo json_encode($ret);
        }

    }


}

$frontend = new Frontend();

if (!isset($_GET['key'])) {
    echo json_encode("Missing Access Key parameter");
    exit(501);
} elseif ($_GET['key'] != constant("ACCESS_KEY")) {
    echo json_encode("Invalid Access Key");
    exit(502);
} elseif (!isset($_GET['action'])) {
    echo json_encode("Missing Action parameter");
    exit(503);
} else {
    $frontend->checkForValidParams();

}





