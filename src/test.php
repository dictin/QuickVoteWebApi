<?php

/**
 * Created by PhpStorm.
 * User: dictin
 * Date: 17/03/16
 * Time: 9:47 PM
 */
require_once 'CoreAPI.php';

header('Content-type: application/json');

define("ACCESS_KEY", "zeg0d58lNZ0xOIQg1wuwtRwiWUOh3oaAbBGHWDitJuJALl4OKibYYeo74bieDdEF");

class Frontend
{

    private $API;

    public function __construct()
    {

        $this->API = new CoreAPI();
    }

    public function exec($args)
    {

    }

    public function parseQuestionArgs($questionArgs)
    {


    }

    public function checkForValidParams()
    {
        $action = $_GET['action'];

        switch ($action) {
            case "addUser":
                if ((isset($_GET['username'])) && (isset($_GET['password'])) && (isset($_GET['email']))) {
                    $answer = $this->API->addUser($_GET['username'], $_GET['password'], $_GET['email']);
                    echo json_encode($answer);
                } else {
                    echo json_encode("Missing parameter for 'addUser' action");
                }
                break;
            case "login":
                if ((isset($_GET['username'])) && (isset($_GET['password']))) {
                    $answer = $this->API->login($_GET['username'], $_GET['password']);
                    echo json_encode($answer);
                } else {
                    echo json_encode("Missing parameter for 'login' action");
                }
                break;
            case "delUser":
                if ((isset($_GET['username'])) && (isset($_GET['token']))) {
                    $answer = $this->API->delUser($_GET['username'], $_GET['token']);
                    echo json_encode($answer);
                } else {
                    echo json_encode("Missing parameter for 'delUser' action");
                }
                break;
            case "addGuestUser":
                $answer = $this->API->addGuestUser();
                echo json_encode($answer);
                break;
            case "addQuestion":
                if ((isset($_GET['username'])) && (isset($_GET['token']))) {
                    $json_args = json_decode('{"questionStr":"foo","comment":"bar","isHidden":true,"acceptsMany":true,"isClosed":true,"questionType":2,"isPrivate":false,"password":null,"ownerID":"dictin","tags":["foo"]}');
                    $answer = $this->API->addQuestion($json_args->questionStr, $json_args->comment, $json_args->isHidden, $json_args->acceptsMany, $json_args->isClosed, $json_args->questionType, $json_args->isPrivate, $json_args->password, $_GET['username'], $json_args->tags, $_GET['token']);
                    echo json_encode(true);
                } else {
                    echo json_encode("Missing parameter for 'AddQuestion' action");
                }
                break;
            default:
                $ret = "Unknown action '" . $action . "'";
                echo json_encode($ret);
        }

    }


}

$API = new CoreAPI();

$json_args = json_decode('{"questionStr":"foo","comment":"bar","isHidden":true,"acceptsMany":true,"isClosed":true,"questionType":2,"isPrivate":false,"password":null,"ownerID":"dictin","tags":["foo"]}');
$API->searchByTag("o", array("foo", "bar"), null, "");








