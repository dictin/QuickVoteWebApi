<?php

/**
 * Created by PhpStorm.
 * User: Propriétaire
 * Date: 2016-04-04
 * Time: 20:57
 */
class Util
{
    public static function filter($string)
    {

        $output = htmlspecialchars($string);
        $output = strip_tags($output);

        return $output;

    }

    public static function genRandomString($length = 10)
    {
        $string = '';
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        for ($i = 0; $i < $length; $i++) {
            $string .= $chars[mt_rand(0, strlen($chars) - 1)];
        }

        return $string;
    }

}